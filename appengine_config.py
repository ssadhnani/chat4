from gaesessions import SessionMiddleware

# suggestion: generate your own random key using os.urandom(64)
# WARNING: Make sure you run os.urandom(64) OFFLINE and copy/paste the output to
# this file.  If you use os.urandom() to *dynamically* generate your key at
# runtime then any existing sessions will become junk every time you start,
# deploy, or update your app!
import os
COOKIE_KEY = 'r\xeef=\xa2\x08\xefH\xf3v\x1c,\xa0\x0c\xb5\xee%P\x86^z\xdccX\xb8\x14\x1c\xfdYZ)\xc1\x89\x9e\xb5\xaf\xd6\x97d\xbb\x85\xfc=K\x0f;\x13\xe2y\xb7\x13=\xbbSP\xc3\xf0\xda\xdc\xdd\xa5U]'

def webapp_add_wsgi_middleware(app):
  from google.appengine.ext.appstats import recording
  app = SessionMiddleware(app, cookie_key=COOKIE_KEY)
  app = recording.appstats_wsgi_middleware(app)
  return app
