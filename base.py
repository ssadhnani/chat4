import webapp2
import jinja2
import os

template_env = jinja2.Environment(loader=jinja2.FileSystemLoader(os.getcwd()))

class Base(webapp2.RequestHandler):

	def render( self, file_name, template_values = {}):
		template = template_env.get_template(file_name)
		self.response.out.write(template.render(template_values))
