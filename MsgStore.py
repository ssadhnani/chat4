from google.appengine.ext import db

class MsgStore(db.Model):
	sender = db.StringProperty()
	receiver = db.StringProperty()
	msg = db.StringProperty()
	msg_time = db.DateTimeProperty(verbose_name=None, auto_now=False, auto_now_add=False)