import base
from oauth2client.client import OAuth2WebServerFlow
import UserList
import datetime
import MsgStore
from gaesessions import get_current_session
import logging
import json
from google.appengine.api import channel
from google.appengine.ext import db

flow = OAuth2WebServerFlow(client_id='973157361069-nttdgrei5cghsq7g9ajufejh43tlr3tm.apps.googleusercontent.com',
						   client_secret='1PHb4Rne0Nt38UgcYh3M70kz',
						   scope='https://www.googleapis.com/auth/userinfo.email',
						   redirect_uri='http://localhost:8080/return')

logging.info('logs')

class LoginPage(base.Base):
	def get(self):
		session = get_current_session()
		self.render('login.html')

class Login(base.Base):
	
	def get(self):
		auth_url = flow.step1_get_authorize_url()
		self.redirect(auth_url)

class Channel(base.Base):
	def create_channel(self):
		session = get_current_session()
		user = session['email']
		time_stamp = str(datetime.datetime.now())
		channel_client_id = user + time_stamp
		session['channel_client_id'] = channel_client_id
		prev_user = UserList.UserList().all().filter("user = ",user).fetch(10)
		prev_user[0].channel_client_id = channel_client_id
		prev_user[0].put()
		token = channel.create_channel(channel_client_id)
		return (token, channel_client_id)
	def post(self):
		token , channel_client_id = create_channel()
		self.response.content_type = 'application/json'
		token_obj = { 'token': token , 'channel_client_id': channel_client_id }
		self.response.write(json.dumps(token_obj))


class Return(base.Base):
	
	def get(self):
		code = self.request.get('code',None)
		if code :
			credentials = flow.step2_exchange(self.request.get("code"))
			email = credentials.id_token['email']
			email = str(email)
			session = get_current_session()
			session['email'] = email
			email_set = {email}
			prev_user = UserList.UserList().all().filter("user = ",email)
			user_set = set()
			for items in prev_user:
				user_set.add(items.user)
			if email_set - user_set == email_set :
				user_list_obj = UserList.UserList( user = email, channel_client_id = '')
				user_list_key = user_list_obj.put()
			self.redirect('/chatlist')
		else :
			self.redirect('/')

class ChatList(base.Base):
	def get(self):
		session = get_current_session()
		current_user = session['email']
		# chosen_user = self.request.get('chosen_user',None)
		# if chosen_user :
		# 	session['chosen_email'] = chosen_user
		# 	self.redirect('/chatbox')
		# else:
		# current_user = session['email']		
		# channel_class_obj = Channel()
		# token = channel_class_obj.create_channel()
		# token_temp_val = {'token':token}
		user_list_obj = UserList.UserList().all().filter("user != ",current_user)
		template_values = {'data':user_list_obj, 'user':current_user }
		#template_values.update(token_temp_val)
		self.render('chatlist.html',template_values)

class ChatBox(base.Base):
	
	def post(self):
		session = get_current_session()
		current_user = session['email']
		chosen_user = self.request.get('chosen')
		latest = self.request.get('latest')
		send_msg_obj = MsgStore.MsgStore().all().order('msg_time').filter("sender = ", current_user).filter("receiver = ", chosen_user).fetch(100)
		received_msg_obj = MsgStore.MsgStore().all().order('msg_time').filter("sender = ", chosen_user).filter("receiver = ", current_user).fetch(100)
		latest = datetime.datetime.strptime('2016-07-10 14:25:55.727174','%Y-%m-%d %H:%M:%S.%f')
		if len(send_msg_obj) > 0 :
			if send_msg_obj[-1].msg_time > latest :
				latest = send_msg_obj[-1].msg_time
		if len(received_msg_obj) > 0 :
			if received_msg_obj[-1].msg_time > latest :
				latest = received_msg_obj[-1].msg_time
		latest_key = {'latest2':latest}
		chosen_key = {'chosen':chosen_user}
		key = {'data': send_msg_obj}
		key2 = {'dada': received_msg_obj}
		key.update(key2)
		key.update(latest_key)
		key.update(chosen_key)
		channel_class_obj = Channel()
		token , channel_client_id = channel_class_obj.create_channel()
		token_temp_val = {'token':token, 'channel_client_id': channel_client_id }
		key.update(token_temp_val)
		self.render('chatbox.html',key)

class PollMsgs(base.Base):
	def post(self):
		session = get_current_session()
		current_user = session['email']
		chosen_user = self.request.get('chosen')
		latest = self.request.get('latest_msg_time')
		# print type(latest)
		latest = datetime.datetime.strptime(latest,'%Y-%m-%d %H:%M:%S.%f')
		# print type(latest)
		# print latest
		send_msg_obj = MsgStore.MsgStore().all().order('msg_time').filter("sender = ", current_user).filter("receiver = ", chosen_user).filter("msg_time >", latest).fetch(100)
		received_msg_obj = MsgStore.MsgStore().all().order('msg_time').filter("sender = ", chosen_user).filter("receiver = ", current_user).filter("msg_time >", latest).fetch(100)
		send_msg_list = []
		received_msg_list = []
		for items in send_msg_obj :
		
			send_msg_list.append(items.msg)
			if items.msg_time > latest :
				latest = items.msg_time
			
		for item in received_msg_obj :
			
			received_msg_list.append(item.msg)
			if item.msg_time > latest :
				latest = item.msg_time
		self.response.content_type = 'application/json'
		obj = { 'send': send_msg_list, 'send_length':len(send_msg_list), 'received': received_msg_list, 'received_length':len(received_msg_list), 'latest' : str(latest) }#if latest else datetime.datetime.now() }
		self.response.write(json.dumps(obj))

class AddMsg(base.Base):
	
	def post(self):
				
		msg = self.request.get("msg",None)
		msg_time = datetime.datetime.now()
		session = get_current_session()
		sender = session['email']
		receiver = self.request.get('chosen')
		receiver_db_obj = UserList.UserList().all().filter("user = ",receiver).fetch(10)
		receiver_client_id = receiver_db_obj[0].channel_client_id
		if msg != None :
			msg_store_obj = MsgStore.MsgStore( msg = msg, msg_time = msg_time, sender = sender, receiver = receiver)
			msg_store_key = msg_store_obj.put()
			# logging.info('logs1')
			channel.send_message(session['channel_client_id'],'hi')
			# print session['channel_client_id']
			# logging.info('logs2')
			channel.send_message(receiver_client_id,'hi')
			# logging.info('logs3')
			# print receiver_client_id


		

class Logout(base.Base):
	def get(self):
		session = get_current_session()
		if session.is_active():
			session.terminate()
		self.redirect('/')